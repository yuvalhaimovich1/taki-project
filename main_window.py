import tkinter as tk
from tkinter import messagebox
import threading
from login_window import open_login_window
from register_window import open_register_window
#from mygame import start_game  # Make sure this is the function you use to start your Pygame
from taki import *
from common import *
from time import sleep

def create_main_window():
    main_window = tk.Tk()
    main_window.title("Main Menu")
    main_window.geometry("400x300")

    def enable_game_button(name):
        register_button['state']    = 'disabled'
        login_button['state']       = 'disabled'
        logout_button['state']      = 'normal'
        game_button['state']        = 'normal'
        main_window.title("Main Menu " + name)

    def disable_game_button():
        register_button['state']    = 'normal'
        login_button['state']       = 'normal'
        logout_button['state']      = 'disabled'
        game_button['state']        = 'disabled'
        main_window.title("Main Menu")

    def run_game():
        #game_thread = threading.Thread(target=start_game)
        #game_thread.start()
        #start ticTacToe
        Globals.dstName=text_area.get("1.0", tk.END+"-1c")
        app = Pyg("Hello")
        app.start()
            

    def logout():
        # Server communication
        msgDst = "server"
        username = Globals.username
        msgSrc = username
        msgType= "logout"
        msgData=["stam"]
        msgSend = buildMsg(msgDst,msgSrc,msgType,msgData)
        send_q.put(msgSend)

        regFlag=False
        while regFlag==False:
            if recv_q.empty() == False: 
                msgRecv = recv_q.get()
                print("777197",msgRecv)
                parts           = msgRecv.split(",")
                if parts[0] == "False" : 
                    print("9999","we logout successful")
                    Globals.username = "" #releasr the name of the user of this client
                    disable_game_button()
                    messagebox.showinfo("Logout Successful", "You are now logout!")
                    regFlag=True
                else:
                     messagebox.showerror("Logout Failed", "???")
                     regFlag=True #do not wait here since it will stuck tkinter
            sleep(0.02) #sleep 20 ms - let the cpu breath        




    register_button = tk.Button(main_window, text="Register", command=lambda: open_register_window(main_window, enable_game_button))
    register_button.pack(pady=10)

    login_button = tk.Button(main_window, text="Login", command=lambda: open_login_window(main_window, enable_game_button))
    login_button.pack(pady=10)

    logout_button = tk.Button(main_window, text="Logout", state='disabled', command=logout)
    logout_button.pack(pady=10)

    game_button = tk.Button(main_window, text="Game", state='disabled', command=run_game)
    game_button.pack(pady=10)

    # Create a Text widget
    text_area = tk.Text(main_window, height=2, width=20)
    text_area.pack()
    text_area.insert(tk.END, "before press Game\nchange to game pair.\n")


    main_window.mainloop()

if __name__ == "__main__":
    create_main_window()

# register_window.py
import tkinter as tk
from tkinter import messagebox
from common import *
from time import sleep

import sqlite3
import bcrypt  # For hashing passwords


# Function to connect to the SQLite database
def connect_db():
    conn = sqlite3.connect('user_database.db')
    cursor = conn.cursor()
    return conn, cursor


# Function to create the users table if it doesn't exist
def create_user_table(cursor):
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            email TEXT NOT NULL,
            password TEXT NOT NULL,
            bornYear TEXT NOT NULL,
            gender TEXT NOT NULL
            
            
        )
    ''')


def open_register_window(parent_window, enable_game_button_callback):
    # Disable the main window
    parent_window.attributes('-disabled', True)



    # Function to insert a new user into the database
    def insert_user(conn, cursor, username, email, password, bornYear, gender):
        conn, cursor = connect_db()
        create_user_table(cursor)

        username = username_entry.get()
        password = password_entry.get()
        bornYear = birthdate_entry.get()
        email = email_entry.get()
        gender = gender_entry.get()
        print(username, password, bornYear, email, gender)
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        cursor.execute('''
            INSERT INTO users (username, password, bornYear ,email, gender )
            VALUES (?, ?, ?, ?, ?)
        ''', (username, email, hashed_password, bornYear, gender))
        conn.commit()
        print("User inserted successfully!")

        # Server communication
        msgDst = "server"
        msgSrc = username
        msgType = "register"
        msgData = [username, password, bornYear, email, gender]
        msgSend = buildMsg(msgDst, msgSrc, msgType, msgData)
        send_q.put(msgSend)
        regFlag = False
        while regFlag == False:
            if recv_q.empty() == False:
                msgRecv = recv_q.get()
                print("772477", msgRecv)
                parts = msgRecv.split(",")
                if parts[0] == "True":
                    print("9999", "we register successful")
                    Globals.username = username  # save the name of the user of this client
                    messagebox.showinfo("Registration Successful", "You are now registered!")

                    # Close the register window and re-enable the main window
                    on_register_window_close()
                    parent_window.attributes('-disabled', False)
                    enable_game_button_callback(username)  # Enable the game button on successful registration
                    regFlag = True
                else:
                    messagebox.showerror("Registration Failed", "Incorrect username or password.")
                    regFlag = True  # do not wait here since it will stuck tkinter
            sleep(0.02)  # sleep 20 ms - let the cpu breath

            # Close the database connection
            conn.close()


        

    def on_register_window_close():
        # Re-enable the main window when the register window is closed
        parent_window.attributes('-disabled', False)
        register_window.destroy()

    register_window = tk.Toplevel(parent_window)
    register_window.title("Register")
    register_window.geometry("300x250")
    register_window.protocol("WM_DELETE_WINDOW", on_register_window_close)

    tk.Label(register_window, text="Username:").pack()
    username_entry = tk.Entry(register_window)
    username_entry.pack()

    tk.Label(register_window, text="Password:").pack()
    password_entry = tk.Entry(register_window, show="*")
    password_entry.pack()

    tk.Label(register_window, text="Birthdate (DD/MM/YYYY):").pack()
    birthdate_entry = tk.Entry(register_window)
    birthdate_entry.pack()

    tk.Label(register_window, text="Email:").pack()
    email_entry = tk.Entry(register_window)
    email_entry.pack()

    tk.Label(register_window, text="Gender:").pack()
    gender_entry = tk.Entry(register_window)
    gender_entry.pack()

    tk.Button(register_window, text="Register", command=attempt_register).pack()

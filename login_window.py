# login_window.py
import tkinter as tk
from tkinter import messagebox
from common import *
from time import sleep

def open_login_window(parent_window, enable_game_button_callback):
    parent_window.attributes('-disabled', True)


    def attempt_login():
        # Here, you'd collect the data and send it to the server
        # For now, just print it to the console or use a messagebox to simulate registration
        username = username_entry.get()
        password = password_entry.get()
        print(username, password)
        
         # Server communication
        msgDst = "server"
        msgSrc = username
        msgType= "login"
        msgData=[username,password]
        msgSend = buildMsg(msgDst,msgSrc,msgType,msgData)
        send_q.put(msgSend)
        regFlag=False
        while regFlag==False:
            if recv_q.empty() == False: 
                msgRecv = recv_q.get()
                print("777127",msgRecv)
                parts           = msgRecv.split(",")
                if parts[0] == "True" : 
                    print("9999","we login successful")
                    Globals.username = username #save the name of the user of this client
                    messagebox.showinfo("Login Successful", "You are now logined!")
        
                    # Close the login window and re-enable the main window
                    close_login_window()
                    parent_window.attributes('-disabled', False)
                    enable_game_button_callback(username)  # Enable the game button on successful registration
                    regFlag=True
                else:
                     messagebox.showerror("Login Failed", "Incorrect username or password.")
                     regFlag=True #do not wait here since it will stuck tkinter
            sleep(0.02) #sleep 20 ms - let the cpu breath           
        


    def close_login_window():
        parent_window.attributes('-disabled', False)  # Re-enable the main window
        login_window.destroy()

    login_window = tk.Toplevel(parent_window)
    login_window.title("Login")
    login_window.geometry("300x150")
    login_window.protocol("WM_DELETE_WINDOW", close_login_window)  # Bind custom close function

    # Login form setup...
    tk.Label(login_window, text="Username:").pack()
    username_entry = tk.Entry(login_window)
    username_entry.pack()

    tk.Label(login_window, text="Password:").pack()
    password_entry = tk.Entry(login_window, show="*")
    password_entry.pack()

    tk.Button(login_window, text="Login", command=attempt_login).pack()

import tkinter as tk
import pygame
import threading
import socket

# Constants for window dimensions
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600

# Initialize Pygame
pygame.init()

class PygameThread(threading.Thread):
    def _init_(self):
        threading.Thread._init_(self)
        self.daemon = True
        self.screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
        self.clock = pygame.time.Clock()
        self.running = True

    def run(self):
        while self.running:
            self.clock.tick(30)  # Cap the frame rate at 30 FPS
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
            # Clear screen and draw game elements
            self.screen.fill((255, 255, 255))
            # Draw game elements here (cards, board, etc.)
            pygame.display.flip()

    def stop(self):
        self.running = False

def start_game():
    pygame_thread = PygameThread()
    pygame_thread.start()

def main():
    root = tk.Tk()
    root.title("Taki Game")

    start_button = tk.Button(root, text="Start Taki Game", command=start_game)
    start_button.pack(pady=20)

    root.mainloop()

if _name_ == "_main_":
    main()
